﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;

namespace JSON_Converter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            this.AllowDrop = true;
            this.DragEnter += Form1_DragEnter;
            this.DragDrop += Form1_DragDrop;

            ContextMenuStrip rightClick = new ContextMenuStrip();

            ToolStripMenuItem rcClear = new ToolStripMenuItem("Clear");
            rcClear.Click += rcClear_Click;

            rightClick.Items.AddRange(new ToolStripItem[] { rcClear });
            textBoxContent.ContextMenuStrip = rightClick;

        }

        void rcClear_Click(object sender, EventArgs e)
        {
            textBoxContent.Text = "";
        }

        void Form1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        void Form1_DragDrop(object sender, DragEventArgs e)
        {
            textBoxPath.Text = "";

            bool userInformed = false;

            string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

            foreach (string file in files)
            {
                if (file.Substring(file.Length - 4).ToLower() == "json")
                {
                    textBoxContent.Text += file + "\r\n" + validateJson(file) + "\r\n\r\n";
                }
                else if (Directory.Exists(file) && !userInformed)
                {
                    MessageBox.Show("In order to fix files from specific folder (and its subfolders) paste the path into the text field.", "Folder drag and drop not supported", MessageBoxButtons.OK);
                    userInformed = true;
                }
            }
        }

        private List<String> getJsonFiles(string path)
        {
            List<String> files = new List<String>();

            if (Directory.Exists(path))
            {
                DirectoryInfo currFolder = new DirectoryInfo(path);

                textBoxContent.Text += " == Converting JSON to XML ==\r\n\r\n";

                foreach (FileInfo file in currFolder.GetFiles("*.json"))
                {
                    string valMsg = validateJson(file.FullName);

                    if (valMsg == "file is valid!")
                    {
                        StringBuilder jsonContent = new StringBuilder();

                        foreach (string line in File.ReadLines(file.FullName, Encoding.UTF8))
                        {
                            jsonContent.AppendLine(line);
                        }

                        string json = jsonContent.ToString();

                        files.Add(json);
                        Console.WriteLine("converted file: " + path);
                    }
                    else
                    {
                        textBoxContent.Text += "\r\n" + file.FullName + "\r\nFILE NOT VALID: \r\n" + valMsg + "\r\n\r\n";
                    }

                }


                textBoxContent.Text += "\r\n";
            }

            return files;
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            string path = textBoxPath.Text;

            if (Directory.Exists(path)){

                DirectoryInfo currFolder = new DirectoryInfo(path);

                textBoxContent.Text += " == Converting JSON to XML ==\r\n\r\n";

                foreach (FileInfo file in currFolder.GetFiles("*.json"))
                {
                    string valMsg = validateJson(file.FullName);

                    if (valMsg == "file is valid!")
                    {
                        StringBuilder jsonContent = new StringBuilder();

                        foreach (string line in File.ReadLines(file.FullName, Encoding.UTF8))
                        {
                            jsonContent.AppendLine(line);
                        }

                        string json = jsonContent.ToString();

                        XNode node = JsonConvert.DeserializeXNode(json, "json");

                        node.Document.Save(file.FullName + ".xml");

                        textBoxContent.Text += "converted: " + file.FullName + "\r\n";
                    }
                    else
                    {
                        textBoxContent.Text += "\r\n" + file.FullName + "\r\nFILE NOT VALID: \r\n" + valMsg + "\r\n\r\n";
                    }

                }


                textBoxContent.Text += "\r\n";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string path = textBoxPath.Text;

            if (Directory.Exists(path))
            {
                DirectoryInfo currFolder = new DirectoryInfo(path);

                textBoxContent.Text += " == Converting XML to JSON ==\r\n\r\n";

                foreach (FileInfo file in currFolder.GetFiles("*.xml"))
                {
                    XmlDocument xml = new XmlDocument();
                    xml.Load(file.FullName);

                    string json = JsonConvert.SerializeXmlNode(xml, Newtonsoft.Json.Formatting.Indented, true);

                    Regex regex = new Regex("^\"\\?xml.*?\\{(.|\\s)*?\\}");

                    json = regex.Replace(json, "");

                    //json.Replace("\"?xml\": {\r\n  \"@version\": \"1.0\",\r\n  \"@encoding\": \"utf-8\"\r\n}", "");

                    File.WriteAllText(file.FullName + ".json", json, Encoding.UTF8);

                    textBoxContent.Text += "converted: " + file.FullName + "\r\n";

                }

                textBoxContent.Text += "\r\n";
            }
        }

        private void buttonValidate_Click(object sender, EventArgs e)
        {
            textBoxContent.Text = "";

            string path = textBoxPath.Text;

            if (Directory.Exists(path))
            {
                DirectoryInfo currFolder = new DirectoryInfo(path);

                textBoxContent.Text += " == Validating JSON ==\r\n\r\n";

                foreach (FileInfo file in currFolder.GetFiles("*.json"))
                {
                    string filePath = file.FullName;

                    textBoxContent.Text += filePath + "\r\n" + validateJson(filePath) + "\r\n\r\n";

                }

                textBoxContent.Text += "\r\n";
            }
        }

        private string validateJson(string filePath)
        {
            StringBuilder jsonContent = new StringBuilder();
            string errorMsg = "";

            foreach (string line in File.ReadLines(filePath, Encoding.UTF8))
            {
                jsonContent.AppendLine(line);
            }

            string json = jsonContent.ToString();
            JObject jOb = null;
            try
            {
                jOb = JObject.Parse(json);
            }
            catch (Exception ex)
            {
                errorMsg = ex.Message;
            }

            if (jOb != null)
            {
                errorMsg = "file is valid!";
            }

            return errorMsg;
        }

        private void buttonAnal_Click(object sender, EventArgs e)
        {
            string path = textBoxPath.Text;

            List<String> jsonFiles = getJsonFiles(path);

            foreach (string json in jsonFiles)
            {
                XmlDocument xml = DocumentExtensions.ToXmlDocument((XDocument)Json2Xml(json));

                foreach (XmlNode node in xml.SelectSingleNode("/*").ChildNodes)
                {
                    Console.WriteLine(node.Name);
                }
            }

            
        }

        private LinkedList<JObject> getJsonFilesFromPath(string path)
        {
            LinkedList<JObject> jsonFiles = new LinkedList<JObject>();

            if (Directory.Exists(path))
            {
                DirectoryInfo currFolder = new DirectoryInfo(path);

                textBoxContent.Text += " == Analysing JSON ==\r\n\r\n";

                foreach (FileInfo file in currFolder.GetFiles("*.json"))
                {
                    string valMsg = validateJson(file.FullName);

                    if (valMsg == "file is valid!")
                    {
                        StreamReader reader = File.OpenText(file.FullName);

                        jsonFiles.AddLast((JObject)JToken.ReadFrom(new JsonTextReader(reader)));

                        textBoxContent.Text += "loading: " + file.FullName + "\r\n";
                    }
                    else
                    {
                        textBoxContent.Text += "\r\n" + file.FullName + "\r\nFILE NOT VALID: \r\n" + valMsg + "\r\n\r\n";
                    }

                }


                textBoxContent.Text += "\r\n";
            }

            return jsonFiles;
        }

        private XNode Json2Xml(string json)
        {
            XNode node = JsonConvert.DeserializeXNode(json, "json");

            return node;
        }
    }
}
